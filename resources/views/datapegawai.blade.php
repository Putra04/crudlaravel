@extends('layout.admin')
@push('css')
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.rtl.min.css" integrity="sha384-DOXMLfHhQkvFFp+rWTZwVlPVqdIhpDVYT9csOnHSgWQWPX0v5MCGtjCJbY6ERspU" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

@endpush
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="mb-5">Data Pegawai</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

      <div class="container">
    <a href="/tambahpegawai" class="btn btn-success">Tambah +</a>
    <!-- {{ Session::get('halaman_url') }} -->
          <div class="row g-3 align-items-center mt-2">
          
            <div class="col-auto">
            <form action="/pegawai" method="GET">
              <input type="search" id="inputPassword6" name="search" class="form-control" aria-describedby="passwordHelpInline">
            </form>
            </div>
              <div class="d-grid gap-2 d-md-flex justify-content-md-end">
              <a href="/exportpdf" class="btn btn-danger me-md-2">Export PDF</a>
              <a href="/exportexcel" class="btn btn-success ">Export Excel</a>
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Import Data</button>
           </div>


          <!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="/importexcel" method="POST" enctype="multipart/form-data"> 
        @csrf

      <div class="modal-body">
        <div class="form-group">
            <input type="file" name="file" required>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
      </form>
  </div>
</div>

        <!-- <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            
        </div> -->
          </div>

        <div class="row">
            <!-- @if ($message = Session::get('success'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif -->

    <table class="table mt-2">
  <thead>

    
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Foto</th>
      <th scope="col">Jenis Kelamin</th>
      <th scope="col">No telfon</th>
      <th scope="col">Tanggal lahir</th>
      <th scope="col">Agama</th>
      <th scope="col">Dibuat</th>
      <th scope="col">Aksi</th>
    </tr>
    
  </thead>
  <tbody>
    @php
        $no = 1;
    @endphp

    @foreach ($data as $index => $row)
    <tr>
      <th scope="row">{{ $index + $data->firstItem() }}</th>
      <td>{{ $row->nama }}</td>
      <td>
            <img src="{{ asset('fotopegawai/'.$row->foto) }}" alt="" style="width: 100px;" >
      </td>
      <td>{{ $row->jeniskelamin }}</td>
      <td>0{{ $row->notelfon }}</td>
      <td>{{ $row->tanggal_lahir }}</td>
      <td>{{ $row->religions->nama }}</td>
      <td>{{ $row->created_at->format('D M Y') }}</td>
      <td>
            <a href="/tampilkandata/{{ $row->id }}" class="btn btn-info">Edit</button>
            <a href="#" class="btn btn-danger delete" data-id="{{ $row->id }}" data-nama="{{ $row->nama }} ">Delete</button>
      </td>
    </tr>
    @endforeach
    
    </div>
        </div>
  </tbody>
</table>

    {{ $data->links() }}

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
   

</html>


<html lang="an"> 
  <head>
  <div class="container">
        
  
</head>
</html>


    </div>
  

@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" 
    crossorigin="anonymous"></script>

    <script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" 
    integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    -->
  </body>

  <script>
    $('.delete').click( function(){
            var pegawaiid = $(this).attr('data-id');
            var nama = $(this).attr('data-nama');

            swal({
              title: "Yakin ?",
              text: "Kamu akan menghapus data pegawai dengan nama "+nama+" ",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })

            .then((willDelete) => {
              if (willDelete) {
                window.location = "/delete/"+pegawaiid+""
                swal("Data berhasil dihapus!", {
              icon: "success",
            });

              } else {
              swal("Data tidak jadi dihapus");
          }
          });
          });
      
  </script>

<script>
  @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
  @endif
</script>

@endpush