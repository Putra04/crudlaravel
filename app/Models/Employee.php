<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Religion;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $dates = ['creeated_at','tanggal_lahir'];


    public function religions(){
        return $this->belongsTo(Religion::class, 'id_religions','id');
    }
}
